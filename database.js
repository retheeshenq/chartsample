var mysql = require("mysql");
var jsonData;
async function init() {
  const fs = require("fs");
  let jsonName = "employees.json";
  let rawdata = await fs.readFileSync(jsonName);
  jsonData = JSON.parse(rawdata);
  connectDB();
}

function connectDB() {
  var con = mysql.createConnection({
    host: "localhost",
    user: "admin",
    password: "w2ww2w",
    database: "allocation"
  });

  con.connect(function(err) {
    if (err) throw err;
    console.log("Connected!");
    executeUpdate(con);
  });
}

function executeUpdate(con) {
  let sql =
    "INSERT INTO `employee`(`employee_record_id`, `employee_id`, `employee_name`, `employee_role`, `employee_email`, `legal_entity_id`, `home_country`, `home_state`, `home_city`, `joining_date`, `practice`, `reporting_manager_id`, `status`, `start_date`, `end_date`, `created_by`, `create_date`, `last_updated_by`, `last_update_date`) VALUES";
  let values = "";
  jsonData.employees.map((item, index, source) => {
    values +=
      "(0," +
      (isNaN(item.employeeno)?100:item.employeeno) +
      ",'" +
      item.name +
      "','" +
      item.c_designation +
      "','" +
      item.email +
      "','Enquero INDIA','INDIA','Karnataka','Bangalore','2018-01-01','RPA'," +
      1067 +
      ",'Active','2018-01-01','2018-01-01'," +
      0 +
      ",'2018-01-01'," +
      0 +
      ",'2018-01-01'),";
  });
  sql = sql + values;
  con.query(sql, function(err, result) {
    if (err) throw err;
    console.log("Result: " + result);
  });
}

init();
