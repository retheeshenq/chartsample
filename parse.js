const data = require("./data.json");
let chart2Data = parseData(data);
console.log(JSON.stringify(chart2Data));
function parseData(data) {
  let chartData = {};
  chartData.hoe = [];
  chartData.hoa = [];
  chartData.hov = [];
  chartData.values = [];
  let listOrderItems = data["OrderReport"]["OrderItem"];
  listOrderItems.forEach(element => {
    chartData.hoe.push(parseInt(element.QtyDifference)); //HO Estimated
    chartData.hoa.push(parseInt(element.QtyActual)); //HO Actual
    chartData.hov.push(parseInt(element.QtyOrdered)); //HO Variance
    chartData.values.push(element.Code);
  });
  return chartData;
}
